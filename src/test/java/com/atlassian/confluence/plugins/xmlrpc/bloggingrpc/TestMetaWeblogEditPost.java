package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelPermissionEnforcer;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.AuthenticationFailedException;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.Hashtable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestMetaWeblogEditPost {
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private LabelPermissionEnforcer labelPermissionEnforcer;

    @Mock
    private ConfluenceUser user;

    private MetaWeblogImpl metaWeblog;

    @Before
    public void setUp() {
        metaWeblog = new MetaWeblogImpl(bloggingUtils, new TransactionTemplate() {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback) {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, labelManager, labelPermissionEnforcer, applicationProperties);

        when(bloggingUtils.getText(anyString(), ArgumentMatchers.<String[]>any())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );
    }

    @After
    public void tearDown() {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        applicationProperties = null;
        user = null;
    }

    @Test
    public void testEditNonExistentPost() throws AuthenticationFailedException {
        when(bloggingUtils.authenticateUser(anyString(), anyString())).thenReturn(user);

        try {
            metaWeblog.editPost(String.valueOf(0), "", "", null, true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.blog.doesnotexists", re.getMessage());
        }
    }

    @Test
    public void testEditRestrictedPost() throws AuthenticationFailedException {
        BlogPost post = new BlogPost();
        post.setSpace(new Space("TST"));

        when(bloggingUtils.authenticateUser(anyString(), anyString())).thenReturn(user);
        when(pageManager.getBlogPost(0L)).thenReturn(post);

        try {
            metaWeblog.editPost(String.valueOf(0), "", "", null, true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.permission.edit.space.blog", re.getMessage());
        }
    }

    @Test
    public void testEditPost() throws RemoteException {
        BlogPost post = new BlogPost();

        post.setSpace(new Space("TST"));
        post.setBodyAsString("");
        post.setCreationDate(new Date());

        final String title = "title";
        post.setTitle(title);

        when(bloggingUtils.authenticateUser(anyString(), anyString())).thenReturn(user);
        when(pageManager.getBlogPost(0L)).thenReturn(post);
        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, post.getSpace(), user)).thenReturn(true);

        metaWeblog.editPost(String.valueOf(0), "", "", new Hashtable<String, Object>() {
            {
                put(MetaWeblogImpl.TITLE, title + " edited");
                put(MetaWeblogImpl.DESCRIPTION, "content");
            }
        }, true);

        verify(pageManager).saveContentEntity(argThat(
                blogPost -> StringUtils.equals(title + " edited", blogPost.getTitle())
                        && StringUtils.equals("content", blogPost.getBodyAsString())
        ), any(), any());
    }

    @Test
    public void testRenamePostToSomethingElseWithTheSameTitle() throws RemoteException {
        BlogPost post = new BlogPost();

        post.setSpace(new Space("TST"));
        post.setBodyAsString("");
        post.setCreationDate(new Date());

        final String title = "title";
        post.setTitle(title);

        when(bloggingUtils.authenticateUser(anyString(), anyString())).thenReturn(user);
        when(pageManager.getBlogPost(0L)).thenReturn(post);
        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, post.getSpace(), user)).thenReturn(true);

        BlogPost conflictingPost = new BlogPost();
        conflictingPost.setTitle(title + " edited");
        when(pageManager.getBlogPost(eq(post.getSpaceKey()), eq(title + " edited"), any())).thenReturn(
                conflictingPost
        );

        try {
            metaWeblog.editPost(String.valueOf(0), "", "", new Hashtable<String, Object>() {
                {
                    put(MetaWeblogImpl.TITLE, title + " edited");
                    put(MetaWeblogImpl.DESCRIPTION, "content");
                }
            }, true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.blog.duplicate", re.getMessage());
        }
    }
}