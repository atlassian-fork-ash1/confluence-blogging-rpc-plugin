package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelPermissionEnforcer;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestMetaWeblogNewMedia {
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private TransactionTemplate transactionTemplate;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private LabelPermissionEnforcer labelPermissionEnforcer;

    private MetaWeblogImpl metaWeblog;

    @Before
    public void setUp() {
        metaWeblog = new MetaWeblogImpl(bloggingUtils, transactionTemplate, spaceManager, spacePermissionManager,
                pageManager, permissionManager, labelManager, labelPermissionEnforcer, applicationProperties);
    }

    @After
    public void tearDown() {
        bloggingUtils = null;
        transactionTemplate = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        applicationProperties = null;
    }

    @Test(expected = NotImplementedException.class)
    public void testNewMediaNotSupported() throws RemoteException {
        metaWeblog.newMediaObject(String.valueOf(0), "", "", null);
    }
}