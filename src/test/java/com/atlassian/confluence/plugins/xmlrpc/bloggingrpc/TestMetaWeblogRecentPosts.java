package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelPermissionEnforcer;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestMetaWeblogRecentPosts {
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private LabelPermissionEnforcer labelPermissionEnforcer;

    private MetaWeblogImpl metaWeblog;

    @Before
    public void setUp() {
        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );

        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://localhost:1990/confluence");

        metaWeblog = new MetaWeblogImpl(bloggingUtils, new TransactionTemplate() {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback) {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, labelManager, labelPermissionEnforcer, applicationProperties);
    }

    @After
    public void tearDown() {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        applicationProperties = null;
    }

    @Test
    public void testGetRecentPostsFromNonExistentSpace() {
        try {
            metaWeblog.getRecentPosts("", "", "", 10);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.space.unknown", re.getMessage());
        }
    }

    @Test
    public void testGetRecentPostsFromRestricedSpace() {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);

        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        try {
            metaWeblog.getRecentPosts(spaceKey, "", "", 10);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.permission.view.space.blog", re.getMessage());
        }
    }

    @Test
    public void testZeroRecentPosts() throws RemoteException {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);

        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(spacePermissionManager.hasPermission(eq(SpacePermission.VIEWSPACE_PERMISSION), eq(space),
                Matchers.anyObject())).thenReturn(true);

        assertEquals(new Vector(), metaWeblog.getRecentPosts(spaceKey, "", "", 0));
    }

    @Test
    public void testRecentPosts() throws RemoteException {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        int noOfPosts = 10;

        BlogPost recentPost = new BlogPost();
        recentPost.setSpace(space);
        recentPost.setTitle("Title");
        recentPost.setBodyAsString("Content");
        recentPost.setCreationDate(new Date());

        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(spacePermissionManager.hasPermission(eq(SpacePermission.VIEWSPACE_PERMISSION), eq(space),
                Matchers.anyObject())).thenReturn(true);
        when(pageManager.getRecentlyAddedBlogPosts(noOfPosts, space.getKey())).thenReturn(
                Arrays.asList(recentPost)
        );
        when(bloggingUtils.convertStorageFormatToView(recentPost)).thenReturn(recentPost.getBodyAsString());

        Vector posts = metaWeblog.getRecentPosts(spaceKey, "", "", noOfPosts);
        assertNotNull(posts);
        assertEquals(1, posts.size());

        Hashtable post = (Hashtable) posts.get(0);
        assertEquals(recentPost.getIdAsString(), post.get(MetaWeblogImpl.POSTID));
        assertEquals(recentPost.getTitle(), post.get(MetaWeblogImpl.TITLE));
        assertEquals(recentPost.getBodyAsString(), post.get(MetaWeblogImpl.DESCRIPTION));
    }
}