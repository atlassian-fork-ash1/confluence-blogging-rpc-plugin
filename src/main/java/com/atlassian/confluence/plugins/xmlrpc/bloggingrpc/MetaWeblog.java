package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.rpc.RemoteException;

import java.util.Hashtable;
import java.util.Vector;

/**
 * Definition of the MeatWeblog API as an Interface. Refer to the <a
 * href="http://www.xmlrpc.com/metaWeblogApi">MetaWeblog API</a> for a
 * description of each methods functionality.
 *
 * @since 1.2.5
 */
public interface MetaWeblog {

    String newPost(String blogid, String username, String password, Hashtable<String, Object> struct, boolean publish)
            throws RemoteException;

    boolean editPost(String postid, String username, String password, Hashtable<String, Object> struct, boolean publish)
            throws RemoteException;

    Hashtable<String, Object> getPost(String postid, String username, String password) throws RemoteException;

    Hashtable<String, Hashtable<String, String>> getCategories(String blogid, String username, String password) throws RemoteException;

    Vector<Hashtable<String, Object>> getRecentPosts(String blogid, String username, String password, int numberOfPosts)
            throws RemoteException;

    Hashtable<String, Object> newMediaObject(String blogid, String username, String password, Hashtable struct)
            throws RemoteException;

}
